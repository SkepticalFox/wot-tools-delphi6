unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Packed_Section, Menus, XMLIntf, XmlDoc, ComCtrls;

type
  TForm1 = class(TForm)
    MemoTest: TMemo;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Help1: TMenuItem;
    Open1: TMenuItem;
    Exit1: TMenuItem;
    About1: TMenuItem;
    OpenDialog1: TOpenDialog;
    StatusBar1: TStatusBar;
    SaveDialog1: TSaveDialog;
    Save1: TMenuItem;
    procedure Open1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure MemoTestChange(Sender: TObject);
    procedure Save1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure UnpackFile(Path:string);
var
        PS:TPacked_Section;
        dictionary:TStringList;
        xDoc:IXMLDOCUMENT;
        xmlroot:IXMLNODE;
        reader:TFileStream;
        buff:Integer;
        text: string;
const
        Binary_Header=$62A14E45;
begin
        xDoc := NewXMLDocument;

        xmlroot:=xDoc.AddChild('root');

        reader:= TFileStream.Create(Path, fmOpenRead OR fmShareDenyWrite);

        reader.ReadBuffer(buff, 4);

        if (buff = Binary_Header) then
        begin
                PS:=TPacked_Section.Create;

                dictionary:=TStringList.Create;

                dictionary:=PS.readDictionary(@reader);

                if (not PS.Error) then
                        PS.readElement(@reader, @xmlroot, @xDoc, @dictionary);
        end;

        FreeAndNil(reader);

        text:=xDoc.XML.Text;

        xDoc:=nil;

        buff:=Pos(#$D#$A, text);
        
        if (buff>0) then
                Delete(text, 1, buff+1);

        text:=xmlDoc.FormatXMLData(xmlDoc.XMLStringToWideString(text));

        Form1.MemoTest.Lines.Text:=text;
end;

procedure TForm1.Open1Click(Sender: TObject);
begin

        if OpenDialog1.Execute then
                UnpackFile(OpenDialog1.FileName);
end;

procedure TForm1.Save1Click(Sender: TObject);
begin
        if SaveDialog1.Execute then
                MemoTest.Lines.SaveToFile(SaveDialog1.FileName);
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
        Close;
end;

procedure TForm1.About1Click(Sender: TObject);
begin
        ShowMessage('WoT Mod Tools'+#13+#10+
        'Author: ShadowHunterRUS'+#13+#10+
        'Version from: 04.05.2015');
end;

procedure TForm1.MemoTestChange(Sender: TObject);
begin
        StatusBar1.Panels[0].Text:='length: ' + IntToStr(Length(Form1.MemoTest.Lines.Text)) + '   lines: ' + IntToStr(Form1.MemoTest.Lines.Count);
end;

end.
