unit Packed_Section;

interface

uses Classes, SysUtils, XMLIntf, XmlDoc, Dialogs;


type
        TElementDescriptor = Record
                nameIndex:integer;
                end_:integer;
                type_:integer;
end;

type
	TPacked_Section = class(TObject)
        readError: Boolean;

	private
        function readStringTillZero(Preader: Pointer):string;
        function readLittleEndianShort(Preader: Pointer):SmallInt;
        function readLittleEndianInt(Preader: Pointer):Longint;
        function readLittleEndianInt64(Preader: Pointer):Int64;
        function ReadChar(Preader: Pointer):Char;
        function ReadSByte(Preader: Pointer):ShortInt;
        function readNumber(Preader: Pointer; lengthInBytes:integer):string;
        function readFloats(Preader: Pointer; lengthInBytes:integer):string;
        function readBoolean(Preader: Pointer; lengthInBytes:integer):Boolean;
        function readLittleEndianFloat(Preader: Pointer):Single;
        function readBase64(Preader: Pointer; lengthInBytes:integer):string;
        function readData(Preader: Pointer; Pdictionary:Pointer; Pelement:Pointer; PxDoc:Pointer; offset:integer; type_:integer; end_:integer):integer;
        function readString(Preader: Pointer; lengthInBytes:integer):string;

	public
        function readDictionary(Preader: Pointer):TStringList;
        procedure readElement(Preader: Pointer; Pelement: Pointer; PxDoc:Pointer; Pdictionary:Pointer);
        property Error: Boolean read readError default False;
end;

var
        intToBase64:Array[0..63] of char = ( 'A','B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y','z', '0', '1', '2', '3', '4', '5', '6','7', '8', '9','+', '/' );

implementation

{ TPacked_Section }


function TPacked_Section.ReadChar(Preader: Pointer):Char;
var
        c:Char;
        reader:^TFileStream;
begin
        reader:=Preader;
        reader.Position:=reader.Position+1;
        reader.Read(c, 1);
        reader.Position:=reader.Position-1;
        Result:=c;
end;

function TPacked_Section.readString(Preader: Pointer; lengthInBytes:integer):string;
var
        reader:^TFileStream;
begin
        reader:=Preader;
        SetLength(Result, lengthInBytes);
        if lengthInBytes > 0 then
                reader.ReadBuffer(Result[1], lengthInBytes);
end;

function TPacked_Section.readStringTillZero(Preader: Pointer):String;
var
	work:String;
	c:Char;
begin
        c:=ReadChar(Preader);
        while (c <> #00) do
        begin
                work:=Concat(work, c);
                c:=ReadChar(Preader);
        end;
        Result:=work;
end;


function TPacked_Section.readDictionary(Preader: Pointer):TStringList;
var
        dictionary:TStringList;
        text:string;
        reader:^TFileStream;
begin
        reader:=Preader;
        dictionary:=TStringList.Create;
        readError:=False;
        try
                text:=readStringTillZero(Preader);

                while (length(text) <> 0) do
                begin
                        dictionary.Add(text);
                        text:=readStringTillZero(Preader);
                end;

                reader.Position:=reader.Position+1;
        except
                readError:=True;
        end;

        Result:=dictionary;
end;

function TPacked_Section.readLittleEndianShort(Preader: Pointer):SmallInt;
var
        LittleEndianShort:SmallInt;
        reader:^TFileStream;
begin
        reader:=Preader;
        reader.ReadBuffer(LittleEndianShort, 2);
        Result:=LittleEndianShort;
end;

function TPacked_Section.readLittleEndianInt(Preader: Pointer):Longint;
var
        LittleEndianInt:Longint;
        reader:^TFileStream;
begin
        reader:=Preader;
        reader.ReadBuffer(LittleEndianInt, 4);
        Result:=LittleEndianInt;
end;

function TPacked_Section.readLittleEndianInt64(Preader: Pointer):Int64;
var
        LittleEndianInt64:Int64;
        reader:^TFileStream;
begin
        reader:=Preader;
        reader.ReadBuffer(LittleEndianInt64, 8);
        Result:=LittleEndianInt64;
end;

function TPacked_Section.ReadSByte(Preader: Pointer):ShortInt;
var
        sbyte:ShortInt;
        reader:^TFileStream;
begin
        reader:=Preader;
        reader.Readbuffer(sbyte,1);
        Result:=sbyte;
end;

function TPacked_Section.readNumber(Preader: Pointer; lengthInBytes:integer):string;
var
        Number:string;
begin
        Case lengthInBytes of
                0: Number:='0';
                1: Number:=IntToStr(ReadSByte(Preader));
                2: Number:=IntToStr(readLittleEndianShort(Preader));
                4: Number:=IntToStr(readLittleEndianInt(Preader));
                8: Number:=IntToStr(readLittleEndianInt64(Preader));
        else raise Exception.Create('Unsupported int size!');
        end;
        Result:=Number;
end;

function TPacked_Section.readLittleEndianFloat(Preader: Pointer):Single;
var
        LittleEndianFloat:Single;
        reader:^TFileStream;
        Bytes: Array[0..3] of Byte;
begin
        reader:=Preader;
        reader.ReadBuffer(Bytes, 4);
        LittleEndianFloat:=Single(Bytes);
        Result:=LittleEndianFloat;
end;

function byteArrayToBase64(a: array of Byte; aLen:Integer):string;
var
        numFullGroups,
        numBytesInPartialGroup,
        inCursor,
        i:integer;
        byte0,
        byte1,
        byte2:Byte;
        resultStr:string;
begin
        numFullGroups:= Round(aLen / 3 );
        numBytesInPartialGroup:= aLen - (3 * numFullGroups);
        inCursor:= -1;
        for i:=1 to numFullGroups do
        begin
                Inc(inCursor);
                byte0:=(a[inCursor] AND $FF);
                Inc(inCursor);
                byte1:=(a[inCursor] AND $FF);
                Inc(inCursor);
                byte2:=(a[inCursor] AND $FF);
                resultStr:=Concat(resultStr, intToBase64[byte0 shr 2]);
                resultStr:=Concat(resultStr, intToBase64[(byte0 shl 4) AND $3f or (byte1 shr 4)]);
                resultStr:=Concat(resultStr, intToBase64[(byte1 shl 2) AND $3f or (byte2 shr 6)]);
                resultStr:=Concat(resultStr, intToBase64[byte2 AND $3f]);
        end;

        if (numBytesInPartialGroup <> 0) then
        begin
                Inc(inCursor);
                byte0:=(a[inCursor] AND $FF);
                resultStr:=Concat(resultStr, intToBase64[byte0 shr 2]);
                if (numBytesInPartialGroup = 1) then
                begin
                    resultStr:=Concat(resultStr, intToBase64[(byte0 shl 4) AND $3f]);
                    resultStr:=Concat(resultStr, '==');
                end else
                begin
                        Inc(inCursor);
                        byte1:= a[inCursor] AND $FF;
                        resultStr:=Concat(resultStr, intToBase64[(byte0 shl 4) AND $3f OR (byte1 shr 4)]);
                        resultStr:=Concat(resultStr, intToBase64[(byte1 shl 2) AND $3f]);
                        resultStr:=Concat(resultStr, '=');
                end;
            end;
            Result:=resultStr;
end;


function TPacked_Section.readBase64(Preader: Pointer; lengthInBytes:integer):string;
var
        bytes:Array of Byte;
        i:integer;
begin
        for i:=1 to lengthInBytes do
                bytes[i]:=ReadSByte(Preader);

        Result:=byteArrayToBase64(bytes, lengthInBytes);
end;


function TPacked_Section.readFloats(Preader: Pointer; lengthInBytes:integer):string;
var
        sb:string;
        n, i:integer;
        rFloat:Single;
begin
        n:=round(lengthInBytes/4);
        for i:=0 to n-1 do
        begin
                if (i <> 0) then
                        sb:=Concat(sb, ' ');
                rFloat:=readLittleEndianFloat(Preader);
                DecimalSeparator := '.';
                sb:=Concat(sb, FloatToStrF(rFloat, ffFixed, 10, 6));
        end;
        Result:=sb;
end;

function TPacked_Section.readBoolean(Preader: Pointer; lengthInBytes:integer):Boolean;
var
        bool_:Boolean;
begin
    bool_:=(lengthInBytes = 1);
    if (bool_) then
        if (ReadSByte(Preader) <> 1) then
            begin
                ShowMessage('Boolean error');
                raise Exception.Create('Syntax error in the xml-file');
            end;
    Result:=bool_;
end;

function TPacked_Section.readData(Preader: Pointer; Pdictionary:Pointer; Pelement:Pointer; PxDoc:Pointer; offset:integer; type_,end_:integer):integer;
var
        lengthInBytes:integer;
        str:string;
        strData:TStringList;
        row0,
        row1,
        row2,
        row3:IXMLNODE;
        element: ^IXMLNODE;
begin
        element:=Pelement;

        strData:=TStringList.Create;

        lengthInBytes:=end_ - offset;

        if (type_ = 0) then
        begin
                readElement(Preader, Pelement, PxDoc, Pdictionary);
        end
        else
        if (type_ = 1) then
        begin
                str:=readString(Preader, lengthInBytes);
                if Length(str)>0 then
                        element.Text:=#9 + str + #9
                else    element.Text:=str;
        end
        else
        if (type_ = 2) then
        begin
                element.Text:=#9+readNumber(Preader, lengthInBytes)+#9;
        end
        else
        if (type_ = 3) then
        begin
                str:=readFloats(Preader, lengthInBytes);
                strData.Delimiter := ' ';
                strData.DelimitedText := str;
                if (strData.Count = 12) then
                begin

                        row0:=element.AddChild('row0');
                        row0.Text:= #9 + strData[0] + ' ' + strData[1] + ' ' + strData[2] + #9;
                        row1:=element.AddChild('row1');
                        row1.Text:= #9 + strData[3] + ' ' + strData[4] + ' ' + strData[5] + #9;
                        row2:=element.AddChild('row2');
                        row2.Text:= #9 + strData[6] + ' ' + strData[7] + ' ' + strData[8] + #9;
                        row3:=element.AddChild('row3');
                        row3.Text:= #9 + strData[9] + ' ' + strData[10] + ' ' + strData[11] + #9;

                end else
                begin
                        element.Text:=#9 + str + #9;
                end;
        end else
        if (type_ = 4) then
        begin
                if (readBoolean(Preader, lengthInBytes)) then
                begin
                        element.Text:=#9+'true'+#9;
                end else
                begin
                        element.Text:=#9+'false'+#9;
                end
        end else
        if (type_ = 5) then
        begin
                element.Text:=#9 + readBase64(Preader, lengthInBytes) + #9;
        end else
        if (type_ = 6) then
        begin
                element.Text:=readBase64(Preader, lengthInBytes);
        end else
        if (type_ = 7) then
        begin
                readString(Preader, lengthInBytes);
                element.Text:='TYPE_ENCRYPTED_BLOB is (yet) unsupported!';
                raise Exception.Create('unsupported section TYPE_ENCRYPTED_BLOB!');
        end else
        begin
                ShowMessage('Unknown type: ' + IntToStr(type_));
                raise Exception.Create('Syntax error in the xml-file');
        end;
        Result:=end_;
end;

procedure TPacked_Section.readElement(Preader: Pointer; Pelement: Pointer; PxDoc:Pointer; Pdictionary:Pointer);
var
        childrenNmber:SmallInt;
        offset,
        selfEndAndType:LongInt;
        i:integer;
        selfType,
        selfEnd:integer;

        children:Array[1..10000] of TElementDescriptor;

        xDoc:^IXMLDOCUMENT;
        element:^IXMLNODE;
        dictionary:^TStringList;

        child:IXMLNode;
begin
        if not readError then
                begin
                xDoc:=PxDoc;
                element:=Pelement;
                dictionary:=Pdictionary;
                try
                        childrenNmber:=readLittleEndianShort(Preader);

                        selfEndAndType:=readLittleEndianInt(Preader);

                        selfType:=selfEndAndType shr 28;
                        selfEnd:=selfEndAndType AND $0fffffff;

                        for i:=1 to childrenNmber do
                        begin
                                children[i].nameIndex:=readLittleEndianShort(Preader);
                                selfEndAndType:=readLittleEndianInt(Preader);
                                children[i].end_:=selfEndAndType AND $0fffffff;
                                children[i].type_:=selfEndAndType shr 28;
                        end;

                        offset:=readData(Preader, Pdictionary, Pelement, PxDoc, 0, selfType, selfEnd);

                        for i:=1 to childrenNmber do
                        begin
                                child:=xDoc^.CreateElement(dictionary^[children[i].nameIndex], '');
                                offset:= readData(Preader, Pdictionary, @child, PxDoc, offset, children[i].type_, children[i].end_);
                                element.ChildNodes.Add(child);
                        end;

                except
                        readError:=True;
                end;
        end;
end;

end.